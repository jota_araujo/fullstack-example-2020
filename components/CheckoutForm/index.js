
import { useState } from "react";
import {
  CardElement,
  useElements,
  useStripe
} from '@stripe/react-stripe-js';
import styles from "./CheckoutForm.module.css";


const CARD_ELEMENT_OPTIONS = {
  style: {
    base: {
      color: "#32325d",
      fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
      fontSmoothing: "antialiased",
      fontSize: "16px",
      "::placeholder": {
        color: "#aab7c4",
      },
    },
    invalid: {
      color: "#fa755a",
      iconColor: "#fa755a",
    },
  },
};

const CheckoutForm = () => {
  const [error, setError] = useState(null);
  const stripe = useStripe();
  const elements = useElements();

  const handleChange = (event) => {
    if (event.error) {
      setError(event.error.message);
    } else {
      setError(null);
    }
  }

  // Handle form submission.
  const handleSubmit = async (event) => {
    event.preventDefault();
    const card = elements.getElement(CardElement);
    const result = await stripe.createToken(card)
    if (result.error) {
      setError(result.error.message);
    } else {
      setError(null);
      stripeTokenHandler(result.token);
    }
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
      <div className={styles.FormRow}>
        <label htmlFor="card-element">
          Credit or debit card
        </label>
        <CardElement
          className={styles.StripeElement}
          id="card-element"
          options={CARD_ELEMENT_OPTIONS}
          onChange={handleChange}
        />
        <div className={styles.ElementErrors} role="alert">{error}</div>
      </div>
      <button type="submit">Submit Payment</button>
    </form>
    </div>
  );
}
  
  export default CheckoutForm