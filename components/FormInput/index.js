
import {
    FormErrorMessage,
    FormLabel,
    FormControl,
    Input,
    Button,
  } from "@chakra-ui/core";

export default function FormInput({ id, label, refFunc, errors }) {
  return (
    <FormControl isInvalid={errors.name}>
      <FormLabel htmlFor={id}>{label}</FormLabel>
      <Input name={id} placeholder={id} ref={refFunc} />
      <FormErrorMessage>{errors[id] && error[id].message}</FormErrorMessage>
    </FormControl>
  );
}
