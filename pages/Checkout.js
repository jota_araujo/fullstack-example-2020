import Head from "next/head";
import Stripe from "stripe";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import { parseCookies, setCookie } from "nookies";
import styles from "../styles/Checkout.module.css";
import CheckoutForm from "../components/CheckoutForm";

const stripePromise = loadStripe(process.env.NEXT_PUBLIC_PUBLISHABLE_KEY);

const fakePrices = [
  { id: "1", price: 1500 },
  { id: "2", price: 3500 },
];

export const getServerSideProps = async ({ query, req }) => {
  const stripe = new Stripe(process.env.SECRET_KEY);

  const { paymentIntentId } = await parseCookies(req);

  if (paymentIntentId) {
    paymentIntent = await stripe.paymentIntents.retrieve(paymentIntentId);

    return {
      props: {
        paymentIntent,
      },
    };
  }

  const filtered = fakePrices.filter((el) => el.id === query.productId);
  if (!filtered.length) {
    return {
      props: {
        purchaseError: "Produto inválido",
      },
    };
  }

  const paymentIntent = await stripe.paymentIntents.create({
    amount: filtered[0].price,
    currency: "brl",
  });

  setCookie(req, "paymentIntentId", paymentIntent.id);

  return {
    props: {
      paymentIntent,
    },
  };
};

function onSubmit(values) {
  setTimeout(() => {
    alert(JSON.stringify(values, null, 2));
  }, 1000);
}

export default function CheckoutPage({ paymentIntent }) {

  //console.log(paymentIntent);
  return (
    <div className={styles.container}>
      <Head>
        <title>Checkout</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Elements stripe={stripePromise}>
          <CheckoutForm paymentIntent={paymentIntent} />
        </Elements>
      </main>
    </div>
  );
}
