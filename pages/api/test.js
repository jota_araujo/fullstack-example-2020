import { PrismaClient } from "@prisma/client"

const db = new PrismaClient()

export default async(req, res) => {

    const result = await db.post.create({
        data: {
          title: "Second post",
          content: "Hello world."
        }
      })

    res.statusCode = 200
    res.end(JSON.stringify(result))
  }