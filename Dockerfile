FROM node:14.8.0

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json yarn.lock ./

RUN yarn install

COPY ./prisma ./prisma

RUN uname -a

RUN yarn prisma generate

COPY . .

CMD [ "yarn", "dev"]